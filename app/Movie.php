<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
	protected $fillable = [
        'title', 'description', 'image','year','length','rating','trailer'
    ];
    public function genres()
    {
        return $this->belongsToMany('App\Genre', 'movie_genres', 'movie_id', 'genre_id')->withTimestamps();;
    }
    public function convertToHoursMins($time, $format = '%02d:%02d') {
    if ($time < 1) {
        return;
    }
    $hours = floor($time / 60);
    $minutes = ($time % 60);
    return sprintf($format, $hours, $minutes);
}
    
}

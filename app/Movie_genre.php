<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie_genre extends Model
{
    protected $fillable = [
        'id', 'title'
    ];
}

<?php

namespace App\Http\Controllers;

use App\Movie_genre;
use Illuminate\Http\Request;

class MovieGenreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Movie_genre  $movie_genre
     * @return \Illuminate\Http\Response
     */
    public function show(Movie_genre $movie_genre)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Movie_genre  $movie_genre
     * @return \Illuminate\Http\Response
     */
    public function edit(Movie_genre $movie_genre)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Movie_genre  $movie_genre
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Movie_genre $movie_genre)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Movie_genre  $movie_genre
     * @return \Illuminate\Http\Response
     */
    public function destroy(Movie_genre $movie_genre)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Movie;
use App\Genre;
use App\Movie_genre;
use Illuminate\Http\Request;

class MovieController extends Controller
{



    public function __construct()
    {
        $this->middleware('auth.admin')->except(['index', 'show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $movies = Movie::paginate(4);
        
        return view('movie.index', compact('movies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = array();

        $genres = Genre::all();
        foreach($genres as $genre){
             $data[] =  $genre->id;
        }
        $genres = Genre::all()->sortBy('title');
        return view('movie.form', compact('genres', 'data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|max:255',
            'description' => 'required|max:255',
            'image' => 'required|max:255',
            'length' => 'required|integer',
            'rating' => 'required|integer',
            'trailer' => 'required|max:255',
            'year' => 'required|integer'
        ]);
        
        Movie::create([
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'image' => $request->get('image'),
            'length' => $request->get('length'),
            'rating' => $request->get('rating'),
            'trailer' => $request->get('trailer'),
            'year' => $request->get('year')
        ]);
        $movie = Movie::all()->sortByDesc('id')->first();
        

        $movie->genres()->sync($request->genre);

        return redirect()->route('movies.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function show(Movie $movie)
    {
        return view('movie.show', compact('movie'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function edit(Movie $movie)
    {

        
        $genres = Genre::all()->sortBy('title');

        $data = array();
        foreach($movie->genres as $genre){
             $data[] =  $genre->id;
        }
             
        return view('movie.form', compact('data', 'movie', 'genres'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Movie $movie)
    {  
        $this->validate($request, [
            'title' => 'required|max:255',
            'description' => 'required|max:255',
            'image' => 'required|max:255',
            'length' => 'required|integer',
            'rating' => 'required|integer',
            'trailer' => 'required|max:255',
            'year' => 'required|integer'
        ]);      
        $movie->update($request->all());
        $movie->genres()->sync($request->genre);
        
        return redirect()->route('movies.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function destroy(Movie $movie)
    {
        
        
        $movie->delete();
        return redirect()->route('movies.index');
    }
}

@extends('layouts.app')

@section('content')
<h1 class="text-center">{{$genre->title}}</h1>
<div class="container genrecontainer">
	<div class="main mainContainer">
		@if (count($genre->movies) > 0)
		@foreach ($genre->movies as $movie)
		
			<div class="row">
				<div class="image">
					<a href="{{route('movies.show', $movie->id)}}">
						<img src="{{ $movie->image }}" width="100%">
					</a>			
				</div>
				<div class="content">
					<h3>{{ $movie->title }} <small> ({{ $movie->year }})</small></h3>
					<p>{{ $movie->description }}</p>
					<p>Movie length: <strong>{{$movie->convertToHoursMins($movie->length, '%0d : %02d')}}</strong> H</p>
					<p>
						Genre :
						@foreach ($movie->genres as $genre)
						{{ $genre->title }}
						@endforeach
					</p>

				</div>
			</div>
		
		@endforeach
		@else
		<h4 class="text-center">No movies with this genre</h4>
		@endif

	</div>
	<div class="sidebar">
		<ul class="list-group text-center">
		@foreach ($genres as $genre)
		  <li><a href="{{route('genres.show', $genre->id)}}">{{ $genre->title}}</a></li>
		@endforeach  
		</ul>
	</div>
</div>

@endsection

@extends('layouts.app')

@section('content')

<div class="container mainContainer showContainer">
	
	<div class="image imageShow">
		<img src="{{$movie->image}}" alt="" width="100%">
	</div>
	<div class="content">
		<h2 class="text-center">{{$movie->title}}<small> ({{ $movie->year }})</small></h2>
        <p>Description: {{$movie->description}}</p>
        <p>Movie length: <strong>{{ $movie->length }}</strong> min.</p>
        <p>
			Genres :
			@foreach ($movie->genres as $genre)
				{{ $genre->title }}
			@endforeach
		</p>
        <button type="button" class="btn btn-primary " data-toggle="modal" data-target=".bs-example-modal-lg">Watch trailer</button>

		<div id="myModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
			<div class="modal-dialog modal-lg" role="document">
			  	<div class="modal-content">
			      <iframe  width="560" height="315" src="{{$movie->trailer}}" frameborder="0" allowfullscreen></iframe>
			    </div>
			</div>
		</div>

		@if(Auth::user() && Auth::user()->isAdmin())
        {{Form::open(['route' => ['movies.destroy', $movie->id], 'method' => 'delete'])}}
        {!!Form::submit('Delete', ['class' => 'btn btn-danger pull-right adminbutton'])!!}
        {!!Form::close()!!}
		<a href="{{route('movies.edit', $movie->id)}}" class="btn btn-default pull-right adminbutton">Edit</a>
		@endif
		
	</div>
</div>    

@endsection

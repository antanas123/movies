@extends('layouts.app')

@section('content')
<div class="container">

@if(isset($movie))
    {{Form::model($movie, ['route' => ['movies.update', $movie->id], 'method' => 'put'])}}
@else
    {{Form::open(['route' => ['movies.store'], 'method' => 'post'])}}
@endif
@if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                   @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
      @endif
<div class="row"> 
    <div class="form-group col-sm-6 col-sm-offset-3">
	<label>Title:</label>
        {!!Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'title'])!!}
    </div>
</div>
<div class="row">
    <div class="form-group col-sm-6 col-sm-offset-3">
	<label>Description:</label>
        {!!Form::textarea('description', null,  ['class' => 'form-control', 'rows' => 5, 'placeholder' => 'dont write too much ..'])!!}
    </div>
</div>
<div class="row">
    <div class="form-group col-sm-6 col-sm-offset-3">
	<label>Image url:</label>
        {!!Form::text('image', null,  ['class' => 'form-control'])!!}
    </div>
</div>
<div class="row">
    <div class="form-group col-sm-6 col-sm-offset-3">
	<label>Trailer url:</label>
        {!!Form::text('trailer', null,  ['class' => 'form-control'])!!}
    </div>
</div>
<div class="row">
    <div class="form-group col-sm-2 col-sm-offset-3">
	<label>Year:</label>
        {!!Form::number('year', null,  ['class' => 'form-control'])!!}
    </div>
    <div class="form-group col-sm-2">
	<label>Length:</label>
        {!!Form::number('length', null,  ['class' => 'form-control'])!!}
    </div>
    <div class="form-group col-sm-2">
	<label>Rating:</label>
        {!!Form::number('rating', null,  ['class' => 'form-control'])!!}
    </div>
</div>
<div class="row">
    <div class="form-group col-sm-6 col-sm-offset-3">
	<label>Genres:</label><br>
        
        
		@foreach ($genres as $genre)
        
			{!! Form::label('genre', $genre->title) !!}
            {!! Form::checkbox('genre[]', $genre->id, in_array($genre->id, $data), [ 'multiple']) !!}
            <br>
		
		@endforeach
		
    </div>
</div>
<div class="row">
    <div class="form-group col-sm-6 col-sm-offset-3 btn-group">
		{!!Form::submit('Save', ['class' => 'btn btn-default'])!!}
        {!!Form::close()!!}
        
    </div>
</div>
        
</div>                
@endsection





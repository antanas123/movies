@extends('layouts.app')

@section('content')
<h1 class="text-center">
	Movies list 
	@if(Auth::user() && Auth::user()->isAdmin())
	<a href="{{route('movies.create')}}">create</a>
	@endif
</h1>
<div class="container mainContainer">

@foreach ($movies as $movie)
<div class="block">
	<div class="row">
		<div class="image">
			<a href="{{route('movies.show', $movie->id)}}">
				<img src="{{ $movie->image }}" width="100%">
			</a>			
		</div>
		<div class="content">
			<h3>{{ $movie->title }} <small> ({{ $movie->year }})</small></h3>
			<p>{{ $movie->description }}</p>
			<p>Movie length: <strong>{{$movie->convertToHoursMins($movie->length, '%0d : %02d')}}</strong> H</p>
			<p>
				Genres :
				@foreach ($movie->genres as $genre)
				<a href="{{route('genres.show', $genre->id)}}">{{ $genre->title}}</a>
				@endforeach
			</p>

		</div>
	</div>
</div>
@endforeach
{{$movies->render()}} 

</div>

@endsection
